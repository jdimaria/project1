#!/afs/nd.edu/user14/csesoft/2017-fall/anaconda3/bin/python3.6
# -*- coding: UTF-8 -*-

## Jack Takazawa, Joey diMaria

import sys
import zmq
from datetime import datetime
import time

context = zmq.Context() # Creates ZMQ context for subscriber to use
socket = context.socket(zmq.SUB) # Opens a socket in "sub" mode

port = sys.argv[1] # Tells the socket what port to connect to

socketName = "tcp://localhost:%s" % port  # Inputs port number into string
socket.connect(socketName)				  # so that socket.connect can use it
print("Connected to", socketName)
socket.setsockopt_string(zmq.SUBSCRIBE, '')

while(1):
	string = socket.recv_string()# Accepts sent message
	print(string)
	time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
	print("Arrived at",time)

