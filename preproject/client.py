#!/afs/nd.edu/user14/csesoft/2017-fall/anaconda3/bin/python3.6
# -*- coding: UTF-8 -*-

#if __name__ == "__main__":
#	print("Not implemented.")	

import sys
import zmq

context = zmq.Context()
socket = context.socket(zmq.SUB)

port = sys.argv[1]

print("Listening...")
socketName = "tcp://localhost:%s" % port 
socket.connect(socketName)

zip_filter = "10001"

if isinstance(zip_filter, bytes):
	zip_filter = zip_filter.decode('ascii')
socket.setsockopt_string(zmq.SUBSCRIBE, zip_filter)

total_temp = 0
update_nbr = 0
while(1):
	string = socket.recv_string()
	zipcode, temperature, relhumidity = string.split()
	total_temp += int(temperature)
	print("Average temperature for zipcode '%s' was %dF" % (zip_filter, total_temp / (update_nbr+1)))
	update_nbr+=1
