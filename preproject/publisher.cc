//Joey DiMaria, Jack Takazawa
//publisher.cc

#include <zmq.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {

	//Prepare context and publisher
	zmq::context_t context (1);
	zmq::socket_t publisher (context, ZMQ_PUB);
	publisher.bind("tcp://*:63750");

	while (1) {
		zmq::message_t message(20);
		string classes = "operating systems"
		snprintf ((char *) message.data(), 20, "%s", classes);
		publisher.send(message);
	}
	return 0;
}
