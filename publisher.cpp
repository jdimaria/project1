//Joey DiMaria, Jack Takazawa
//publisher.cpp

#include <zmq.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <iostream>
#include <csignal>
#include <fstream>
#include <unistd.h>

using namespace std;

#define within(num) (int) ((float) num * random () / (RAND_MAX + 1.0))

int ctrlc = 0;
zmq::context_t context (1);//Creates ZMQ context
zmq::socket_t publisher (context, ZMQ_PUB);	//Sets up socket as a PUBLISHER

//Handles Ctrl-C - currently does enter and outputs message, but zmq closures not working
void signalHandler(int signum) {
	cout << "\nInterrupt signal " << signum << " received. Exiting program\n";
	//zmq_msg_close(*message);
	//publisher.close();			//This is where our signal handling takes place
	//zmq_ctx_destroy(context); 	//The zmq callers don't work, but it exits when called and
	ctrlc = 1;						//lets the user know it has been interrupted
	//zmq_terminate(context);
	exit(0);
}

int main(int argc, char *argv[]) {
	signal(SIGINT, signalHandler);//Sets up signal handler

	if (argc != 2) {
		cout << "Error: Unable to launch, no port specified" << endl;// Exits if no port entered
		cout << "Usage: ./project1 ####  where ####is a valid port number" << endl;
		exit(1);
	}
	int port = stoi(argv[1]);
	if (port < 63700 || port > 63799) { //Exits if invalid port number
		cout << "Bad port number. Choose between 63700 and 63799" << endl;
		exit(1);
	}

	cout << "Welcome to the MPMBS" << endl;
	cout << "Publishing on port " << argv[1] << endl;
	string address = "tcp://*:";
	address += argv[1];
	publisher.bind(address);			//Bind the publisher to selected address
	srandom ((unsigned) time (NULL));
	string line;//Strings for filenames, iterators
	string command = "";
	string ans = "";
	string fileName = "";
	int numChar;

	while (1) {
		cout << "> ";
		numChar = 0;
		cin >> command;
		if (command == "help") {//Print help menu
			cout << "The commands for MPMBS are as follows:" << endl;
			cout << "   help	Show help message" << endl;
			cout << "   send	Send file, will then prompt for name of file or directory path" << endl;
			cout << "   quit	Exit the program" << endl;
		}
		else if (command == "send") {
			cout << "Name of file?: "; 	//Gets name of file to send
			cin >> fileName;
			ifstream file2send (fileName);
			if (file2send.is_open()) {
				while (getline (file2send,line)) {		//Determines if file is too large
					numChar += line.length();
					string s = line;
					int l = line.length();
					for (int i = 0; i < l; i++ ) {
						if (int(line[i]) < 32 || int(line[i]) > 126) {
							cout << "Invalid ASCII characters found in file." << endl;
							continue; //Exits if a non readable ASCII character is found
						}
					}
				}
				file2send.close();
				if (numChar > 120) {					//If file is too large, does not send
					cout << "File too large" << endl;
					continue;
				}
				cout << "The following message will be sent:" << endl;
				ifstream file2send (fileName);
				while (getline (file2send,line)) {		//Displays message to be sent
					cout << line << endl;
				}
				file2send.close();
			}
			else {
				cout << "Error: No file named " << fileName << " in this folder" << endl;
				continue;// If the file does not exist in this folder, backs out
			}
			cout << "Type YES to confirm: ";
			cin >> ans;
			if (ans == "YES" || ans == "yes" || ans == "Yes") {
				ifstream file2send (fileName);
				while (getline (file2send,line)) {		//Runs through file and gets message to be sent
					numChar = line.length()+1;
					zmq::message_t message(numChar);
					string s = line;
					const char *i = s.c_str();//Changes C++ string to C string to send through printf
					snprintf((char *) message.data(), numChar, i);
					publisher.send(message);//Send message to subscribers
				}
				file2send.close();
				cout << "Message sent!" << endl;
			}
			else {
				cout << "Stopping..." << endl; //If user doesn't want to send, type anything
				continue;
			}
		}
		else if (command == "quit") { //Quits
			cout << "Thanks for using the MPMBS" << endl;
			exit(0);
		}
		else { // Prints help statement if invalid command is entered
			cout << "The commands for MPMBS are as follows:" << endl;
			cout << "   help	Show help message" << endl;
			cout << "   send	Send file, will then prompt for name of file or directory path" << endl;
			cout << "   quit	Exit the program" << endl;
		}
		if (ctrlc == 1) {
			break;
		}
	}

	return 0;
}

